//
//  ViewController.swift
//  SwiftScrollView
//
//  Created by Douglas Sass on 1/19/17.
//  Copyright © 2017 Douglas Sass. All rights reserved.
//
//  Created an app that is a scrollview and has 9 different views
//  with different background colors and will be able to scroll through all nine sections
//
//              -----------------------------
//
//              |   1    |   2     |    3   |
//
//              -----------------------------
//
//              |   4    |   5     |    6   |
//
//              -----------------------------
//
//              |   7    |    8    |    9   |
//
//              -----------------------------
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //Sets the size of the viewable area to the appilcation add sets it backgroun color to light gray
        let scrollview = UIScrollView(frame: self.view.bounds)
        scrollview.backgroundColor = UIColor.lightGray
        
        //Adds the scrollview to the super view
        self.view.addSubview(scrollview)
        
        scrollview.isPagingEnabled = true
        
        //Creates the size of the content the views can fill
        scrollview.contentSize = CGSize(width: self.view.bounds.size.width * 3, height: self.view.bounds.size.height * 3)
        
        //Row 1 containing views 1, 2, and 3
        
        //Create View 1 with the background color to purple
        let view1 = UIView(frame: CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: self.view.frame.size.height))
        view1.backgroundColor = UIColor.purple
        
        //Adds subview view1 to the scrollview
        scrollview.addSubview(view1)
        
        
        //Create View 2 with the background color to orange
        let view2 = UIView(frame: CGRect(x: self.view.bounds.size.width, y: 0, width: self.view.bounds.size.width, height: self.view.frame.size.height))
        view2.backgroundColor = UIColor.orange
        
        //Adds subview view2 to the scrollview
        scrollview.addSubview(view2)
        
        
        //Create View 3 with the background color to magenta
        let view3 = UIView(frame: CGRect(x: self.view.bounds.size.width * 2, y: 0, width: self.view.bounds.size.width, height: self.view.frame.size.height))
        view3.backgroundColor = UIColor.magenta
        
        //Adds subview view3 to the scrollview
        scrollview.addSubview(view3)
        
        
        //Row 2 containing views 4, 5, and 6
        
        //Create View 4 with the background color to green
        let view4 = UIView(frame: CGRect(x: 0, y: self.view.frame.size.height, width: self.view.bounds.size.width, height: self.view.frame.size.height))
        view4.backgroundColor = UIColor.green
        
        //Adds subview view4 to the scrollview
        scrollview.addSubview(view4)
        
        
        //Create View 5 with the background color to blue
        let view5 = UIView(frame: CGRect(x: self.view.bounds.size.width, y: self.view.frame.size.height, width: self.view.bounds.size.width, height: self.view.frame.size.height))
        view5.backgroundColor = UIColor.blue
        
        //Adds subview view5 to the scrollview
        scrollview.addSubview(view5)
        
        
        //Create View 6 with the background color to yellow
        let view6 = UIView(frame: CGRect(x: self.view.bounds.size.width * 2, y: self.view.frame.size.height, width: self.view.bounds.size.width, height: self.view.frame.size.height))
        view6.backgroundColor = UIColor.yellow
        
        //Adds subview view6 to the scrollview
        scrollview.addSubview(view6)
        
        
        //Row 3 containing views 7, 8, and 9
        
        //Create View 7 with the background color to darkGray
        let view7 = UIView(frame: CGRect(x: 0, y: self.view.frame.size.height * 2, width: self.view.bounds.size.width, height: self.view.frame.size.height))
        view7.backgroundColor = UIColor.darkGray
        
        //Adds subview view7 to the scrollview
        scrollview.addSubview(view7)
        
        
        //Create View 8 with the background color to black
        let view8 = UIView(frame: CGRect(x: self.view.bounds.size.width, y: self.view.frame.size.height * 2, width: self.view.bounds.size.width, height: self.view.frame.size.height))
        view8.backgroundColor = UIColor.black
        
        //Adds subview view8 to the scrollview
        scrollview.addSubview(view8)
        
        
        //Create View 9 with the background color to red
        let view9 = UIView(frame: CGRect(x: self.view.bounds.size.width * 2, y: self.view.frame.size.height * 2, width: self.view.bounds.size.width, height: self.view.frame.size.height))
        view9.backgroundColor = UIColor.red
        
        //Adds subview view9 to the scrollview
        scrollview.addSubview(view9)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

